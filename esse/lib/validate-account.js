module.exports = {
    usernameIsValid: (username) => {
        return username.length >= 3
            && username.length <= 15
            && username.indexOf(" ") === -1;
    },
    passwordIsValid: (password) => {
        return password.length >= 8 && password.length <= 250;
    },
}
