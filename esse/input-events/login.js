'use strict';

const { EventUtil, Logger } = require("ranvier");
const { usernameIsValid } = require("../lib/validate-account");

module.exports = {
    event: state => (socket, args) => {
        const say = EventUtil.genSay(socket);
        const write = EventUtil.genWrite(socket);
        write("> Enter your account name: ");

        socket.once("data", async data => {
            let username = data.toString("utf8").trim().toLowerCase();

            if (usernameIsValid(username)) {
                let account = null;
                try {
                    account = await state.AccountManager.loadAccount(username);
                } catch (e) {
                    Logger.error(e.message);
                }

                if (account) {
                    // Login
                    return socket.emit("password", socket, { account });
                } else {
                    // Create Account
                    say(`No account '${username}' found!`);
                    write("> Would you like to create this account? <bold><cyan>[y/N]</cyan></bold> ");

                    socket.once("data", data => {
                        let doCreateAccount = data.toString("utf8").trim().toLowerCase() === "y";

                        if (doCreateAccount) {
                            return socket.emit("create-account", socket, { username });
                        } else {
                            return socket.emit("login", socket);
                        }
                    });
                }
            } else {
                say("<bold><red>Invalid username</red></bold>");
                socket.end();
            }
        });
    }
};
