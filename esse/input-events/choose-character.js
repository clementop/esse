'use strict';

const { EventUtil } = require("ranvier");

module.exports = {
    event: state => socket => {
        EventUtil.genSay(socket)("Choose Character");
        socket.end();
    }
};
