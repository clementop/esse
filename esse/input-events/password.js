'use strict';

const { EventUtil } = require("ranvier");
const { passwordIsValid } = require("../lib/validate-account");

const failedAttempts = {};

function addFailedAttempt (account) {
    let attempts = failedAttempts[account.username];
    if (attempts == null) {
        attempts = 1;
    } else {
        attempts += 1;
    }
    failedAttempts[account.username] = attempts;
    return attempts >= 3;
}

function clearFailedAttempt (account) {
    delete failedAttempts[account.username];
}

module.exports = {
    event: state => (socket, args) => {
        const say = EventUtil.genSay(socket);
        const write = EventUtil.genWrite(socket);

        if (args.confirmPassword) {
            write("> Confirm your password: ");
        } else {
            write("> Enter your password: ");
        }
        socket.command("toggleEcho");

        socket.once("data", passwordData => {
            socket.command("toggleEcho");
            say("");
            let password = passwordData.toString().trim();
            let { account } = args;

            if (args.createAccount) {
                if (args.confirmPassword) {
                    if (!account.checkPassword(password)) {
                        say("<red>Passwords do not match.</red>");
                        return socket.emit("password", socket, { ...args, confirmPassword: false });
                    }

                    state.AccountManager.addAccount(account);
                    return socket.emit("choose-character", socket, { account });
                } else {
                    if (password.length === 0) {
                        say("<red>Cancelling account creation</red>");
                        return socket.end();
                    }
                    if (!passwordIsValid(password)) {
                        say("<red>Password must be at least 8 characters</red>");
                        return socket.emit("password", socket, args);
                    }

                    account.setPassword(password);
                    return socket.emit("password", socket, { ...args, confirmPassword: true });
                }
            } else {
                if (!account.checkPassword(password)) {
                    say("<red>Invalid password</red>");
                    if (addFailedAttempt(account)) {
                        clearFailedAttempt(account);
                        return socket.end();
                    }
                    return socket.emit("password", socket, { ...args });
                } else {
                    return socket.emit("choose-character", socket, { account });
                }
            }
        });
    }
};
