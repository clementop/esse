'use strict';

const { EventUtil, Account } = require("ranvier");

module.exports = {
    event: state => (socket, args) => {
        const say = EventUtil.genSay(socket);
        const write = EventUtil.genWrite(socket);

        let { username } = args;
        say(`Creating account '${username}'.`);

        let account = new Account({
            username
        });

        return socket.emit("password", socket, { createAccount: true, account });
    }
};
