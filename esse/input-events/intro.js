'use strict';

const fs = require("fs");
const { EventUtil } = require("ranvier");


module.exports = {
    event: state => socket => {
        const banner = fs.readFileSync(__dirname + "/../resources/banner.txt").toString("utf8");
        EventUtil.genSay(socket)(banner);
        return socket.emit('login', socket);
    }
};
