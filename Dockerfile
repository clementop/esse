FROM node:10

RUN git clone https://github.com/RanvierMUD/ranviermud.git /opt/ranvier

WORKDIR /opt/ranvier
RUN npm ci
RUN npm run install-bundle https://github.com/RanvierMUD/telnet-networking

EXPOSE 4000

CMD ["/bin/bash"]
