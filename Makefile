DOCKER = docker
IMAGE_NAME = registry.gitlab.com/clementop/esse
SRC_MOUNT = /mnt/esse
DATA_MOUNT = /opt/ranvier/data

.PHONY: all image image-deploy run deploy start restart stop

all:
	@echo "run a specific command instead"

image:
	$(DOCKER) build -t $(IMAGE_NAME) .

image-deploy:
	$(DOCKER) login registry.gitlab.com
	$(DOCKER) push $(IMAGE_NAME)

run:
	$(DOCKER) run -it --rm -v $(PWD):$(SRC_MOUNT) -v $(PWD)/data:$(DATA_MOUNT) -w $(SRC_MOUNT) $(IMAGE_NAME)

deploy:
	rm -rf /opt/ranvier/bundles/esse
	cp -r esse /opt/ranvier/bundles
	cp ranvier.json /opt/ranvier/ranvier.json
	cp init.d/ranvier /etc/init.d/ranvier

start:
	service ranvier start

stop:
	service ranvier stop

restart:
	service ranvier restart
