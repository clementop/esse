# Esse

Esse is a Multi-User Dungeon (MUD) where magical implements are enchanted using
the purest essence extracted from living creatures.

## Getting Started

This project is run on the [ranvier](https://ranviermud.com/) MUD engine. Game
content is supplied via the `esse` bundle.

Before moving forward, you will need to install a docker engine. A `Dockerfile`
is included to build a container with all of the needed dependencies, including
node version 10, ranvier, and the ranvier telnet bundle. Either you can build
the image yourself, or the latest version will be downloaded from the GitLab
project's docker registry.

The `Makefile` contains the commands needed to run a development server:
 * `image`      Build the docker image
 * `run`        Run the docker image
 * `deploy`     Run within the docker container.
    Deploys the bundle and config file to the ranvier directory.
 * `start`      Run within the docker container.
    Starts the ranvier server.  
    Logs are streamed to `/var/log/ranvier.log`.
 * `stop`       Run within the docker container.
    Stops the ranvier server.
 * `restart`    Run within the docker container.
    Restarts the ranvier server.
